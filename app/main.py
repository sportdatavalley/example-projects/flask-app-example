from tracemalloc import start
from flask import Flask
import os
import random
import pymysql.cursors

app = Flask(__name__)

DB_HOST = os.getenv('DB_HOST', None)
DB_PORT = os.getenv('DB_PORT', None)
DB_USER = os.getenv('DB_USER', None)
DB_PASS = os.getenv('DB_PASS', None)
DB_NAME = os.getenv('DB_NAME', None)

random.seed()

def start_db_connection():    
    return pymysql.connect(
        host=DB_HOST,
        port=int(DB_PORT),
        user=DB_USER,
        password=DB_PASS,
        database=DB_NAME,
        cursorclass=pymysql.cursors.DictCursor
    )

@app.route('/')
def hello():
    return 'Hello, World!'

@app.route('/myvar')
def myvar():
   myenvvar = os.getenv('EXAMPLE', "not set yet")
   return myenvvar


@app.route('/init_db')
def db_init():
    connection = start_db_connection()
    with connection:
        with connection.cursor() as cursor:
            create_table_sql = "CREATE TABLE IF NOT EXISTS players (id INT AUTO_INCREMENT, number INT, PRIMARY KEY (id))"
            insert_record_sql = "INSERT INTO players (number) VALUES (%s)"

            cursor.execute(create_table_sql)
            cursor.execute(insert_record_sql, (str(random.randint(0, 100))))
        
        connection.commit()
    return "Done"

@app.route('/read_db')
def read_db():
    connection =  start_db_connection()
    result = None
    with start_db_connection() as connection:
        with connection.cursor() as cursor:
            read_sql = "SELECT number FROM players"
            cursor.execute(read_sql)
            result = cursor.fetchall()
            print(result)

    return ','.join(map(lambda r: str(r['number']), result))