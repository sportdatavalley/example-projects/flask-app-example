FROM python:3.8-buster

COPY Pipfile Pipfile.lock /app/

WORKDIR /app

RUN pip install --upgrade pip
RUN pip install pipenv
RUN pipenv install --deploy --system

COPY /app /app/

EXPOSE 80

ENV FLASK_APP=main
CMD ["flask", "run", "--host", "0.0.0.0"]